#ifndef VIDEO_PUBLISHER_ROS_MODULE_H
#define VIDEO_PUBLISHER_ROS_MODULE_H


#include <iostream>

#include <opencv2/opencv.hpp>

#include <ros/ros.h>

#include "droneModuleROS.h"

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>


//Vector
//std::vector
#include <vector>

//String
//std::string, std::getline()
#include <string>

//String stream
//std::istringstream
#include <sstream>

//File Stream
//std::ofstream, std::ifstream
#include <fstream>

//String stream
//std::istringstream
#include <sstream>



class VideoPublisherROSModule : public Module
{
    //Video Capture
protected:
    cv::VideoCapture videoCapture;
    //Frame
protected:
    cv::Mat frame;

    //Frame rate
protected:
    //TODO

    //Video topic name
protected:
    std::string videoTopicName;
public:
    int setVideoTopicName(std::string videoTopicName);

    //Video file
protected:
    std::string videoFile;
public:
    int setVideoFile(std::string videoFile);


    //Publisher
protected:
    image_transport::Publisher imagePubl;
public:
    bool publishImage();



public:
    VideoPublisherROSModule(double moduleRate);
    ~VideoPublisherROSModule();


public:
    void open(ros::NodeHandle & nIn);
    void close();

protected:
    bool init();

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};


#endif // VIDEO_PUBLISHER_ROS_MODULE_H
