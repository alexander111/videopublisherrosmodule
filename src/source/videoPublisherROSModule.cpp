

#include "videoPublisherROSModule.h"

using namespace std;

VideoPublisherROSModule::VideoPublisherROSModule(double moduleRate) : Module(droneModule::active, moduleRate)
{

    return;
}

VideoPublisherROSModule::~VideoPublisherROSModule()
{

    return;
}


int VideoPublisherROSModule::setVideoTopicName(std::string videoTopicName)
{
    this->videoTopicName=videoTopicName;
    return 1;
}

int VideoPublisherROSModule::setVideoFile(std::string videoFile)
{
    this->videoFile=videoFile;
    return 1;
}

void VideoPublisherROSModule::open(ros::NodeHandle &nIn)
{
    //Node
    Module::open(nIn);


    //Init
    if(!init())
    {
        cout<<"Error init"<<endl;
        return;
    }


    //Open Video File
    if(!videoCapture.open(videoFile))
    {
        cout<<"error openning video capture"<<endl;
        return;
    }

    if(!videoCapture.isOpened())  // check if we succeeded
    {
        cout<<"error opened video capture"<<endl;
        return;
    }


    //Publisher -> Image
    image_transport::ImageTransport it(nIn);
    imagePubl = it.advertise( videoTopicName, 1, true);


    //Flag of module opened
    droneModuleOpened=true;

    //autostart
    moduleStarted=true;

    //End
    return;
}

void VideoPublisherROSModule::close()
{
    Module::close();


    //Close video file
    //It is closed automatically


    return;
}

bool VideoPublisherROSModule::init()
{

    //Video file
    std::string video_file;
    ros::param::get("~video_file",video_file);
    cout<<"video_file="<<video_file<<endl;
    if (video_file.length()==0)
    {
        cout<<"[ROSNODE] Error with video_file";
    }


    //video publ topic name
    std::string video_topic_name;
    ros::param::get("~video_topic_name",video_topic_name);
    cout<<"video_topic_name="<<video_topic_name<<endl;
    if (video_topic_name.length()==0)
    {
        cout<<"[ROSNODE] Error with video_topic_name";
    }



    //setters
    if(!this->setVideoTopicName(video_topic_name))
        return 0;

    if(!this->setVideoFile(video_file))
        return 0;


    return true;
}

bool VideoPublisherROSModule::resetValues()
{
    //

    return true;
}

bool VideoPublisherROSModule::startVal()
{
    //Do stuff

    //End
    return Module::startVal();
}

bool VideoPublisherROSModule::stopVal()
{
    //Do stuff

    return true;
}

bool VideoPublisherROSModule::run()
{
    if(!Module::run())
        return false;

    if(droneModuleOpened==false)
        return false;


    //Read frame on file
    videoCapture >> frame;

    if(frame.empty())
    {
        cout<<"Video ended"<<endl;
        return false;
    }


    //Publish
    if(!publishImage())
    {
        cout<<"error publishing"<<endl;
        return false;
    }

    return true;
}



bool VideoPublisherROSModule::publishImage()
{
    //Msg
    cv_bridge::CvImage my_cv_image;
    //Fill header
    my_cv_image.header.stamp = ros::Time::now();
    //Fill image
    my_cv_image.image = frame;
    my_cv_image.encoding = "bgr8";
    sensor_msgs::ImagePtr img_msg = my_cv_image.toImageMsg();

    //Publish
    imagePubl.publish(img_msg);


    return true;
}
