//////////////////////////////////////////////////////
//  videoPublisherROSModule_Node.cpp
//
//  Created on: Jul 25, 2014
//      Author: joselusl
//
//  Last modification on: Jul 25, 2014
//      Author: joselusl
//
//////////////////////////////////////////////////////



//I/O Stream
//std::cout
#include <iostream>

#include <string>


// ROS
//ros::init(), ros::NodeHandle, ros::ok(), ros::spinOnce()
#include "ros/ros.h"



//cv module
#include "videoPublisherROSModule.h"


//Nodes names
//MODULE_NAME_DRONE_KEYPOINTS_GRID_DETECTOR
#include "nodes_definition.h"





using namespace std;

int main(int argc, char **argv)
{
    // Init
    ros::init(argc, argv, MODULE_NAME_DRONE_SIMULATOR_GRID_DETECTOR); //Say to ROS the name of the node and the parameters
    ros::NodeHandle n; //Este nodo admite argumentos!!


    // Constructor
    std::string node_name=ros::this_node::getName();
    cout<<"Starting "<<node_name<<endl;


    //Module rate
//    std::string module_rate_str;
//    ros::param::get("~module_rate",module_rate_str);
//    cout<<"module_rate_str="<<module_rate_str<<endl;
//    double module_rate;
//    std::istringstream convertedValue1(module_rate_str);
//    convertedValue1>>module_rate;
    double module_rate;
    ros::param::get("~module_rate",module_rate);
    cout<<"module_rate="<<module_rate<<endl;


    //Class
    VideoPublisherROSModule MyVideoPublisherROSModule(module_rate);


    //Open
    MyVideoPublisherROSModule.open(n);


    //Loop -> shyncronous Module
    while(ros::ok())
    {
        ros::spinOnce();

        //Run retina
        if(!MyVideoPublisherROSModule.run())
        {
            //cout<<"error"<<endl;
        }

        //Sleep
        MyVideoPublisherROSModule.sleep();
    }

    return 1;
}

