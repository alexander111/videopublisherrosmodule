cmake_minimum_required(VERSION 2.8.3)

set(PROJECT_NAME videoPublisherROSModule)
project(${PROJECT_NAME})

### Use version 2011 of C++ (c++11). By default ROS uses c++98
#see: http://stackoverflow.com/questions/10851247/how-to-activate-c-11-in-cmake
#see: http://stackoverflow.com/questions/10984442/how-to-detect-c11-support-of-a-compiler-with-cmake
#add_definitions(-std=c++11)
#add_definitions(-std=c++0x)
add_definitions(-std=c++03)

# Set the build type.  Options are:
#  Coverage       : w/ debug symbols, w/o optimization, w/ code-coverage
#  Debug          : w/ debug symbols, w/o optimization
#  Release        : w/o debug symbols, w/ optimization
#  RelWithDebInfo : w/ debug symbols, w/ optimization
#  MinSizeRel     : w/o debug symbols, w/ optimization, stripped binaries
#set(ROS_BUILD_TYPE RelWithAsserts)



# VIDEO_PUBLISHER_ROS_MODULE
set(VIDEO_PUBLISHER_ROS_MODULE_SOURCE_DIR
	${PROJECT_SOURCE_DIR}/src/source) 
	
set(VIDEO_PUBLISHER_ROS_MODULE_INCLUDE_DIR
	${PROJECT_SOURCE_DIR}/src/include)

set(VIDEO_PUBLISHER_ROS_MODULE_SOURCE_FILES
        ${VIDEO_PUBLISHER_ROS_MODULE_SOURCE_DIR}/videoPublisherROSModule.cpp
)
 
set(VIDEO_PUBLISHER_ROS_MODULE_HEADER_FILES
        ${VIDEO_PUBLISHER_ROS_MODULE_INCLUDE_DIR}/videoPublisherROSModule.h
)


find_package(catkin REQUIRED
		COMPONENTS roscpp image_transport cv_bridge droneModuleROS)

find_package(OpenCV REQUIRED)



catkin_package(
        DEPENDS OpenCV
        CATKIN_DEPENDS roscpp image_transport cv_bridge droneModuleROS
  )


include_directories(${VIDEO_PUBLISHER_ROS_MODULE_INCLUDE_DIR})
include_directories(${OpenCV_INCLUDE_DIRS})
include_directories(${catkin_INCLUDE_DIRS})



add_library(videoPublisherROSModule ${VIDEO_PUBLISHER_ROS_MODULE_SOURCE_FILES} ${VIDEO_PUBLISHER_ROS_MODULE_HEADER_FILES})
add_dependencies(videoPublisherROSModule ${catkin_EXPORTED_TARGETS})
target_link_libraries(videoPublisherROSModule ${OpenCV_LIBS})
target_link_libraries(videoPublisherROSModule ${catkin_LIBRARIES})


add_executable(videoPublisherNode ${VIDEO_PUBLISHER_ROS_MODULE_SOURCE_DIR}/videoPublisherROSModule_Node.cpp)
add_dependencies(videoPublisherNode ${catkin_EXPORTED_TARGETS})
target_link_libraries(videoPublisherNode videoPublisherROSModule)
target_link_libraries(videoPublisherNode ${OpenCV_LIBS})
target_link_libraries(videoPublisherNode ${catkin_LIBRARIES})

